{/* < > */}
const express = require('express');
const mongoose = require('mongoose');
const TareaSchema = require('./modelos/Tarea')
const User = require('./modelos/User')
const router = express.Router();
//Base de datos 3UnXszY2CszrZPS
mongoose.connect(`mongodb+srv://dbUser:3UnXszY2CszrZPS@cluster0.ww9vu.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`)
//End Base de datos dbuser

//Operaciones CRUD
router.get('/', (req, res)=>{
    res.send({'palabra':"alfondoque"})
}
)
router.get('/tarea', (req, res)=>{
    TareaSchema.find((err, data)=>{
        if(!err){
            res.send(data);
        }else{
            console.log(err);
        }
    })
})
router.post('/tarea', (req, res)=>{
    let {nombre, detalle} = req.body;
    let nuevaTarea = new TareaSchema({
        nombreTarea : nombre,
        detalleTarea : detalle
    });
    nuevaTarea.save((err, data)=>{
        if(err){
            console.log(err);
        }else{
            res.send(data);        }
    })
})
/////////////Reto
router.put('/user/:id', (req, res)=>{
    let id = req.params.id;
    let {tipoDeDocumento, documentoDeIdentificacion,
        nombres, apellidos, direccion, email,
        telefonoFijo, telefonoCelular, enlace,
        descripcionDelPerfil} = req.body;
    User.findByIdAndUpdate(id, {
        tipoDeDocumento: tipoDeDocumento,
        documentoDeIdentificacion: documentoDeIdentificacion,
        nombres: nombres,
        apellidos: apellidos,
        direccion: direccion,
        email: email,
        telefonoFijo: telefonoFijo,
        telefonoCelular: telefonoCelular,
        enlace: enlace,
        descripcionDelPerfil: descripcionDelPerfil,
    }
    ,(err, docs) => {
        if (err) {
            res.send({'message':'Error de actualización'});
        }else{
            User.findById(id, (err, data)=>{
                res.send(data);
            })
        }
    }
    );
})
router.delete('/user/:id', (req, res) => {
    let id = req.params.id;
    User.findByIdAndDelete(id, (err)=>{
        if(!err){
            res.send({'message':'Eliminacion satisfactoria'});
        }else{
            console.log(err);
            res.send({'message':'Eliminacion fracasada'});
        }
    });
})
router.get('/user/:id', (req, res)=>{
    let id = req.params.id;
    User.findById(id, (err, data)=>{
        if(!err){
            res.send(data);
        }else{
            console.log(err);
        }
    })
})
router.get('/user', (req, res)=>{
    User.find((err, data)=>{
        if(!err){
            res.send(data);
        }else{
            console.log(err);
        }
    })
})
router.post('/user', (req, res)=>{
    let {tipoDeDocumento, documentoDeIdentificacion,
        nombres, apellidos, direccion, email,
        telefonoFijo, telefonoCelular, enlace,
        descripcionDelPerfil} = req.body;
    let user = new User({
        tipoDeDocumento: tipoDeDocumento,
        documentoDeIdentificacion: documentoDeIdentificacion,
        nombres: nombres,
        apellidos: apellidos,
        direccion: direccion,
        email: email,
        telefonoFijo: telefonoFijo,
        telefonoCelular: telefonoCelular,
        enlace: enlace,
        descripcionDelPerfil: descripcionDelPerfil,
    });
    user.save((err, data)=>{
        if(err){
            console.log(err);
        }else{
            res.send(data);        }
    })
})
/////////////End reto
//End operaciones CRUD

const app = express();
app.use(express.urlencoded({extended:true}));
app.use(express.json());
app.use(router);
app.listen(
    3000,
    ()=> console.log('Conectado desde el puerto 3000')
)
