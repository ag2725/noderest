const mongoose = require('mongoose');

let TareaSchema = new mongoose.Schema({
    nombreTarea : String,
    detalleTarea : String
})
module.exports = mongoose.model('Tarea', TareaSchema, 'Tareas');