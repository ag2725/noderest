const mongoose = require('mongoose');

let User = new mongoose.Schema({
    tipoDeDocumento : String,
    documentoDeIdentificacion : String,
    nombres : String,
    apellidos : String,
    direccion : String,
    email : String,
    telefonoFijo : String,
    telefonoCelular : String,
    enlace : String,
    descripcionDelPerfil : String,
})
module.exports = mongoose.model('User', User, 'Users');