import axios from "axios";
const actualizarUsuario = ()=>{
    axios.put(`http://192.168.1.2:3000/user/${process.argv[2]}`, {
            tipoDeDocumento : 'DNI',
            documentoDeIdentificacion : '1234567890',
            nombres : 'Juan Carlos',
            apellidos : 'Bodoque',
            direccion : 'CALLE FALSA 123',
            email : 'l@c.com',
            telefonoFijo : '6087700000',
            telefonoCelular : '3134821469',
            enlace : 'https://mongoosejs.com/docs/queries.html',
            descripcionDelPerfil : 'Comparar la filosofía con la teología es como comparar El Amor verdadero con la pornografía',
        }).then((response)=>{
            console.log(response);
        }).catch(function (error) {
            console.log(error);
    });
}
const eliminarUsuario = ()=>{
    process.argv.forEach(() => {
        axios.delete(`http://192.168.1.2:3000/user/${process.argv[2]}`).then((response)=>{
                console.log(response);
            }).catch(function (error) {
                console.log(error);
        });
    
    });
}
const nuevoUsuario = ()=>{
    axios.post('http://192.168.1.2:3000/user', {
            tipoDeDocumento : 'CC',
            documentoDeIdentificacion : '1065123485',
            nombres : 'JUAN CARLOS',
            apellidos : 'ESTUPIÑAN',
            direccion : 'CALLE FALSA 123',
            email : 'l@c.com',
            telefonoFijo : '6087700000',
            telefonoCelular : '3134821469',
            enlace : 'https://mongoosejs.com/docs/queries.html',
            descripcionDelPerfil : 'Comparar la filosofía con la teología es como comparar El Amor verdadero con la pornografía',
        }).then((response)=>{
            console.log(response);
        }).catch(function (error) {
            console.log(error);
    });
}

const nuevaTarea = ()=>{
    axios.post('http://192.168.1.2:3000/tarea', {
            nombre : 'ever',
            detalle : 'detalle de la tarea'
        }).then((response)=>{
            console.log(response);
        }).catch(function (error) {
            console.log(error);
    });
}
const optenerTareas = ()=>{
    axios.get('http://192.168.1.2:3000/tarea')
        .then((response)=>{
            console.log(response);
        }).catch(function (error) {
            console.log(error);
    });
}
actualizarUsuario();
// eliminarUsuario();
// nuevoUsuario();
// nuevaTarea();
// optenerTareas();